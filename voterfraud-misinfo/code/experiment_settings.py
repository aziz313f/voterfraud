event = "voter_fraud"

filter_words = {
    "voter_fraud": {
        "vote", "elect", "ballot", "mail", "congress", "expos",
        "fraud", "steal", "stole", "rig", "cheat", "hack", "discard",
        "donald", "trump", "joe", "biden", "ilhan", "pelosi", "obama",
        "democrat", "republican", "gop", "wwg1wga", "qproof", "qanon",
    },
}

model_names = {
            "msmarco-distilroberta-base-v2",
            "paraphrase-distilroberta-base-v1",
            "stsb-roberta-base",
            "quora-distilbert-base",
            "stsb-roberta-large", 
            "stsb-distilbert-base",
            "paraphrase-distilroberta-base-v1",    
}
cluster_num = 200
