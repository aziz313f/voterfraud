#// Investigating the efficacy of Semi-Supervised learning to generate pseudo labels using US VoterFraud data


Run the `code` in the following order:
- Clean dataset: `python video_cleaner.py`
- Sentence embeddings: `python video_vectorizer.py`
- Cluster embeddings: `python video_cluster.py`
- Evaluate embeddings: `python cluster_evaluator.py`.
- Generate Pseudo Labels: `pseduo_labels.py` and `remove_cluster_from_labels.py`
- Generate Graphs: `plot_graphs.py`

